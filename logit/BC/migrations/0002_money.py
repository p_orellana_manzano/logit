# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20151023_2115'),
        ('BC', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Money',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('revenue', models.DecimalField(max_digits=6, decimal_places=2)),
                ('exprenses', models.DecimalField(max_digits=6, decimal_places=2)),
                ('total', models.DecimalField(max_digits=6, decimal_places=2)),
                ('identifier', models.ForeignKey(related_name='identifier', to='main.Logs')),
            ],
        ),
    ]
