# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0004_auto_20151023_2115'),
        ('BC', '0003_auto_20151025_2058'),
    ]

    operations = [
        migrations.CreateModel(
            name='Money',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('revenue', models.DecimalField(max_digits=6, decimal_places=2)),
                ('exprenses', models.DecimalField(max_digits=6, decimal_places=2)),
                ('total', models.DecimalField(max_digits=6, decimal_places=2)),
                ('identifier', models.ForeignKey(related_name='identifier', to='main.Logs')),
                ('user', models.ForeignKey(related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
