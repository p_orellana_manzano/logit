# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('BC', '0002_money'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='money',
            name='identifier',
        ),
        migrations.DeleteModel(
            name='Money',
        ),
    ]
