from django.db import models
from django.contrib.auth.models import User
from main.models import Logs

from datetime import datetime  

# Create your models here.
class Notification(models.Model):
	created = models.DateTimeField(editable=False, auto_now_add=True)
	content = models.TextField(verbose_name='Notification')
	owner = models.ForeignKey(User, related_name='owner')
	from_notification = models.ForeignKey(User, related_name='creator')
