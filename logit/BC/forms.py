from django.contrib.auth.models import User
from main.models import *
from django import forms

class CircleForm(forms.ModelForm):

	class Meta:
		model = Circles
		fields = ('name', 'description', 'group')

	def __init__(self, *args, **kwargs):
		super(CircleForm, self).__init__(*args, **kwargs)
		self.fields['group'].widget = forms.CheckboxSelectMultiple()

class TinyLogForm(forms.ModelForm):
	class Meta:
		model = tinylog
		fields = ('value', 'who')

class LogForm(forms.ModelForm):
	class Meta:
		model = Logs
		fields = ('description',)