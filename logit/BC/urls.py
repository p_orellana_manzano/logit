from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fmq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
     url(r'^$', 'BC.views.index',name='index'),
     url(r'^search/$', 'BC.views.search', name='search'),
     url(r'^profile/(?P<username>\w+)/$', 'BC.views.profile', name='profile'),
     url(r'^logs/$', 'BC.views.circles', name='circles'),
     url(r'^logs/group/create$', 'BC.views.circle_create', name='circle_create'),
     url(r'^logs/(?P<id>\d+)$', 'BC.views.logs', name='logs'),
     url(r'^logs/(?P<id>\d+)/add/$', 'BC.views.new_log', name='add_log'),
     url(r'^logs/(?P<pid>\d+)/detail/(?P<id>\d+)/$', 'BC.views.log_detail', name='log_detail'),
     url(r'^logs/(?P<pid>\d+)/detail/(?P<id>\d+)/add/$', 'BC.views.log_detail_add', name='log_detail_add'),
   )