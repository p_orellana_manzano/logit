from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, logout, login as auth_login
from main.forms import UserForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def login(request):
	#if request.user.is_authenticated():
		#return HttpResponse("yuss")
	if 'login' in request.POST:
		user = authenticate(username=request.POST['username'], password=request.POST['password'])
		if user is not None:
		    # the password verified for the user
		    if user.is_active:
		    	auth_login(request, user)
		        return HttpResponseRedirect('/')
		    else:
		        login_error_code = 2
		else:
		    # the authentication system was unable to verify the username and password
		    login_error_code = 1
	template = 'main/index.html'
	instance = RequestContext(request, locals())
   	return render_to_response(template, context_instance=instance)

def pass_reset(request):
	template = 'main/pass_reset.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

def signup(request):
	if 'submit_register' in request.POST:
		user_form = UserForm(request.POST)
		pass_validator = validation_pass(request.POST['password'], request.POST['rpassword'])
		if user_form.is_valid() and pass_validator:
			user = user_form.save()
			user.set_password(user.password)
			user.save()
			return HttpResponseRedirect('/')
	template = 'main/signup.html'
	instance = RequestContext(request, locals())
	return render_to_response(template, context_instance=instance)

@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/accounts/login/')


def validation_pass(pass1, pass2):
	if pass1 and pass2:
		if pass1 != pass2:
			return False
		else:
			return True
	return False