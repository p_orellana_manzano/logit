from django.db import models

# Create your models here.
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

####Stores your friends###
class Friendship(models.Model):
    created = models.DateTimeField(auto_now_add=True, editable=False)
    creator = models.ForeignKey(User, related_name="friendship_creator_set")
    friend = models.ForeignKey(User, related_name="friend_set")
    STATUS = (
        ('A', 'ACEPTADO'),
        ('P', 'PENDIENTE'),
        ('N', 'NOAMIGOS'),
    )
    status = models.CharField(max_length=1, choices=STATUS, null=True, default='N')

    def friend_request_sent(self):
        self.status = 'P'
        self.save()

    def friend_request_accepted(self):
        self.status = 'A'
        self.save()

    def unfriend(self):
        self.status = 'N'
        self.save()

    def __unicode__(self):
        return self.friend.username

class Circles(models.Model):
    name = models.CharField(max_length=50, help_text='Name your group!', default=' ')
    description = models.TextField(help_text='Describe your group...', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    group = models.ManyToManyField(Friendship)

    def __unicode__(self):
        return self.name


class Logs(models.Model):
	created = models.DateTimeField(auto_now_add=True, editable=False)
	group = models.ForeignKey(Circles, related_name="log_set")
	description = models.CharField(max_length=100, help_text="This is the grey text")

class tinylog(models.Model):
    log = models.ForeignKey(Logs, related_name="log")
    created = models.DateTimeField(auto_now_add=True, editable=False)
    value = models.DecimalField(max_digits=6, decimal_places=2)
    who = models.CharField(max_length=100)

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
 
    def __unicode__(self):
        return "{}'s profile".format(self.user.username)
 
    class Meta:
        db_table = 'user_profile'
 
    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False                               
 
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
