# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_remove_logs_whom'),
    ]

    operations = [
        migrations.CreateModel(
            name='tinylog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('value', models.DecimalField(max_digits=6, decimal_places=2)),
                ('who', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='logs',
            name='value',
        ),
        migrations.RemoveField(
            model_name='logs',
            name='who',
        ),
        migrations.AddField(
            model_name='tinylog',
            name='log',
            field=models.ForeignKey(related_name='log', to='main.Logs'),
        ),
    ]
