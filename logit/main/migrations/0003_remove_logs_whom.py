# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20151023_1410'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='logs',
            name='whom',
        ),
    ]
