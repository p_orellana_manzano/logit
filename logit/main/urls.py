from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fmq.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^login/$', 'main.views.login', name='login'), #maps url to login 
    url(r'^password/reset/$', 'main.views.pass_reset', name='pass_reset'),
    url(r'^signup/$', 'main.views.signup', name='signup'),
    url(r'^logout/$', 'main.views.logout_view', name='logout'),
   )